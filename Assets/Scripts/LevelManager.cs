﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
    
	public void LoadLevel(string levelName){
		Debug.Log("New Level load: " + levelName);
		SceneManager.LoadScene(levelName);
	}

	public void QuitRequest(){
		Debug.Log("Quit requested");
		Application.Quit();
	}

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void BrickDestroyed()
    {
        if (Brick.BreakabelCount <=0)
        {
            LoadNextLevel();
        }
    }
}
