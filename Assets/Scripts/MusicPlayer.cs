﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private static MusicPlayer _instance = null;

    void Awake()
    {
        Debug.Log("Music Player Awake" + GetInstanceID());
        if (_instance != null)
        {
            GameObject.Destroy(gameObject);
            Debug.Log("Music Player instance self-destroyed!");
        }
        else
        {
            _instance = this;
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }

	// Use this for initialization
	void Start () {
        Debug.Log("Music Player Start" + GetInstanceID());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
