﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Paddle _paddle;
    private bool _hasStarted = false;
    private Vector3 _paddleToBallVector;
    private AudioSource _audio;

    // Use this for initialization
    void Start ()
	{
	    _paddle = GameObject.FindObjectOfType<Paddle>();
	    _paddleToBallVector = transform.position - _paddle.transform.position;
        _audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
	{
        if(!_hasStarted)
            transform.position = _paddle.transform.position + _paddleToBallVector;

	    if (Input.GetMouseButtonDown(0))
	    {
	        print("Mouse pressed, launch ball");
            GetComponent<Rigidbody2D>().velocity = new Vector2(2f, 10f);
	        _hasStarted = true;
	    }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        Vector2  tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
        if(_hasStarted)
            _audio.Play();

        GetComponent<Rigidbody2D>().velocity += tweak;
    }
}
