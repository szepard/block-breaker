﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public Sprite[] HitSprites;
    public AudioClip Smash;
    public static int BreakabelCount = 0;

    private int _timesHit;
    private LevelManager _levelManager;
    private bool _isBreakable;

    // Use this for initialization
    void Start()
    {
        _isBreakable = (this.tag == "Breakable");
        if (_isBreakable)
            BreakabelCount++;
        _timesHit = 0;
        _levelManager = GameObject.FindObjectOfType<LevelManager>();
        print(BreakabelCount);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        AudioSource.PlayClipAtPoint(Smash, transform.position);
        if(_isBreakable)
            HandleHits();
    }

    private void HandleHits()
    {
        int maxHits = HitSprites.Length + 1;
        _timesHit++;
        if (_timesHit >= maxHits)
        {
            BreakabelCount--;
            _levelManager.BrickDestroyed();
            Destroy(gameObject);
            print(BreakabelCount);
        }
        else
        {
            LoadSprites();
        }
    }

    private void LoadSprites()
    {
        int spriteIndex = _timesHit - 1;
        if(HitSprites[spriteIndex])
            this.GetComponent<SpriteRenderer>().sprite = HitSprites[spriteIndex];
    }
}
