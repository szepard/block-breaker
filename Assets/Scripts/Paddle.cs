﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    public bool AutoPlay = false;
    private Ball _ball;

	// Use this for initialization
	void Start ()
	{
	    _ball = GameObject.FindObjectOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (!AutoPlay)
	        MoveWithMouse();
	    else
	        RunAutoPlay();
	}

    private void MoveWithMouse()
    {
        var mousePosInBlocks = Input.mousePosition.x/Screen.width*16;
        transform.position = new Vector3(Mathf.Clamp(mousePosInBlocks, 0.5f, 15.5f), transform.position.y);
    }

    private void RunAutoPlay()
    {
        transform.position = new Vector3(_ball.transform.position.x, transform.position.y);
    }
}
